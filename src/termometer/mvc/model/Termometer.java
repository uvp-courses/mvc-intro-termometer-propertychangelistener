package termometer.mvc.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Termometer {

    public static final String PROP_TEMPERATURA = "temperature";
    private float temperature;

    public Termometer() {
    }

    public Termometer(float temperature) {
        this.temperature = temperature;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperatura(float temperature) {
        float oldTemperature = this.temperature;
        this.temperature = temperature;
        // Después de realizar la modificación de un atributo se procede a
        // notificar el cambio en el mismo.
        propertyChangeSupport.firePropertyChange(PROP_TEMPERATURA, oldTemperature, temperature);
    }

    /**
     * En luhar se tener una lista de manejadores de eventos, se tiene un objeto
     * de tipo PropertyChangeSupport el cual es el encargado de registrar los
     * manejadores de eventos y notificar las modificaciones realizas a los 
     * atributos
     */
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

}
